package question2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RecursionTest {

	/**
	 * Test recursiveCount with n = 0
	 * only 30 and 50 is counted
	 */
	@Test
	void testNIs0() {
		int[] arr = {22,30,40,50,60,1};
		int result = Recursion.recursiveCount(arr, 0);
		assertEquals(2,result);
	}
	
	/**
	 * Test recursiveCount with n = 2
	 * ignores 20 since index is below 2
	 */
	@Test
	void testNIs2() {
		int[] arr = {20,3,5,5,1,2};
		int result = Recursion.recursiveCount(arr, 2);
		assertEquals(0,result);
	}
	
	/**
	 * Test recursiveCount when n = 1
	 * only counts 22
	 * 30 and 40 or on even indexes
	 */
	@Test
	void testNIs1() {
		int[] arr = {4,22,30,5,40,2};
		int result = Recursion.recursiveCount(arr, 1);
		assertEquals(1,result);
	}
	

}
