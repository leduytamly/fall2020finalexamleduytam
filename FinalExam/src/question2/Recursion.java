package question2;

public class Recursion {
	/**
	 * Methods that uses recursion to count all elements in an array that is greater than 20
	 * and is odd
	 * @param arr Integer array
	 * @param n number to count greater than
	 * @return Number of elements that match the conditions
	 */
	public static int recursiveCount(int[] arr, int n) {
		//Base case, if the start is >= to the array length
		if (n >= arr.length) {
			return 0;
		}
		int count = recursiveCount(arr,n+1);
		//when the number is > 20 adds 1 to count
		if(arr[n] > 20 && n%2==1) {
			return count +1;
		}
		//when it doesn't match the two conditions, returns without add + 1 
		else {
			return count;
		}
	}
}
