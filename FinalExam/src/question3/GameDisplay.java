package question3;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Contains the Coin Flip Game
 * @author Le
 *
 */
public class GameDisplay extends VBox{
	
	/**
	 * CoinFlipGame object
	 */
	private CoinFlipGame cfg = new CoinFlipGame();
	
	/**
	 * Constructor for the GameDisplay
	 */
	public GameDisplay() {
		this.getChildren().add(getGame());
	}
	
	/**
	 * Creates the VBox which holds the coin flip game
	 * @return VBox which holds the coin flip game 
	 */
	public VBox getGame() {
		Text money = new Text("Money: 100$");
		TextField bet = new TextField();
		Button heads = new Button("heads");
		Button tails = new Button("tails");
		Text feedback = new Text();
		
		//Adding the event handlers for the buttons
		heads.setOnAction(new CoinFlipChoice(money,bet,feedback,heads.getText(),cfg));
		tails.setOnAction(new CoinFlipChoice(money,bet,feedback,tails.getText(),cfg));
		
		//HBox which contains the 2 buttons
		HBox buttons = new HBox();
		buttons.getChildren().addAll(heads,tails);
		
		//VBox which contains the money, bet TextField, buttons HBox, and feedback message 
		VBox game = new VBox();
		game.getChildren().addAll(money,bet,buttons,feedback);
		
		return game;
	}
}
