package question3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class App extends Application  {	
	public void start(Stage stage) {
		Group root = new Group(); 

		GameDisplay gd = new GameDisplay();
		root.getChildren().add(gd);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);

		//associate scene to stage and show
		stage.setTitle("Coin Flip Game"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
    	Application.launch(args);
    }
}    

