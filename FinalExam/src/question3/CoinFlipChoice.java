package question3;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * Event Handler for the head or tails button
 * @author Le 
 *
 */
public class CoinFlipChoice implements EventHandler<ActionEvent>{
	/**
	 * Text which holds how much money the user has 
	 */
	private Text money;
	
	/**
	 * TextField which holds how much the user wants to bet
	 */
	private TextField bet;
	
	/**
	 * Text which contains the feedback message for the user
	 */
	private Text feedback;
	
	/**
	 * String which contains the choice of the user(heads or tails)
	 */
	private String choice;
	
	/**
	 * The CoinFlipGame object
	 */
	private CoinFlipGame cfg;
	
	/**
	 * Constructor for CoinFlipChoice
	 * @param money Money text
	 * @param bet Bet TextField
	 * @param feedback Feedback Text
	 * @param choice Choice of the user
	 * @param cfg CoinFlipGame object
	 */
	public CoinFlipChoice(Text money,TextField bet,Text feedback,String choice,CoinFlipGame cfg) {
		this.money = money;
		this.bet = bet;
		this.feedback = feedback;
		this.choice = choice;
		this.cfg = cfg;
	}
	
	/**
	 * handle methods which plays a round, and updates the money text and the feedback
	 */
	@Override
	public void handle(ActionEvent arg0) {
		//if the value that was entered is not valid, it won't play the game
		if(isValidBetInput(this.bet.getText())) {
			int betAmount = Integer.valueOf(this.bet.getText());
			String feedback = cfg.playRound(this.choice, betAmount);
			this.feedback.setText(feedback);
			this.money.setText("Money: "+cfg.getMoney()+"$");		
		}
		//Displays an error message if the input was invalid
		else {
			this.feedback.setText("There was an issue please try again");
		}
	}
	
	/**
	 * Checks if the value is valid (number inputed was empty or a string, number inputed is greater than the money they have)
	 * @param bet Player's bet choice
	 * @return boolean value representing if the choice was valid or not
	 */
	public boolean isValidBetInput(String bet) {
		try {
			int betAmount = Integer.valueOf(this.bet.getText());
			//if the bet is greater than the money they have, returns false
			if(betAmount > cfg.getMoney()) {
				return false;
			}
		} catch (NumberFormatException e) {
			//if the number could not be converted to an int, returns false
			return false;
		}
		return true;
	}
}
