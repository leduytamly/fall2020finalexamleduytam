package question3;

import java.util.Random;

/**
 * Contains everything to play a coin flip game
 * @author Le 
 *
 */
public class CoinFlipGame {
	/**
	 * initial money value is 100
	 */
	private int money = 100;
	
	/**
	 * Constant varriable which holds the two possible outcomes for a coin
	 */
	private final String[] COINOPTION = {"heads","tails"};
	
	/**
	 * Random number generator
	 */
	private Random r = new Random();
	
	/**
	 * Getter for money
	 * @return The money that the player has
	 */
	public int getMoney() {
		return this.money;
	}
	
	/**
	 * Flips the coin and adds or subtracts the amount if the user won or lost
	 * @param bet The betting choice of the player
	 * @param betAmount The amount they bet
	 * @return Message saying if the player won or not
	 */
	public String playRound(String bet,int betAmount) {
		//flips the coin (generates a number between 0 and 1)
		String coinValue = this.COINOPTION[r.nextInt(2)];
		//if the coin lands on the same side as their bet, adds the amount to the money variable
		if(coinValue.equals(bet)) {
			this.money+=betAmount;
			return "Coin landed on "+coinValue+" you won!";
		}
		//if the coin does not land on the same side as their bet, subtracts the amount from the money variable
		else {
			this.money-=betAmount;
			return "Coin landed on "+coinValue+" you lose";
		}
	}
}
